import UIKit

class RestaurantTableViewController: UITableViewController, UISplitViewControllerDelegate {
private var model = RestaurantScoreModel()
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.restaurants.count
    }
    
    override func viewDidLoad() {
        splitViewController!.delegate = self
        self.splitViewController!.preferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "RestaurantTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! RestaurantTableViewCell
        
        // Fetches the appropriate restaurant for the data source layout.
        let (restaurant,score) = model.restaurants[indexPath.row]
        
       cell.nameLabel.text = "\(restaurant.name)"
       cell.scoreControl.rating = score
       cell.keukenTypeLabel.text = "\(restaurant.kitchenType)"
       cell.backgroundView = UIImageView(image: UIImage(named: restaurant.photo)!)
       cell.backgroundView?.alpha = 0.4
        
       return cell
    }
    
  
    @IBAction func unwindFromVoted(segue: UIStoryboardSegue) {
        let voteController = segue.sourceViewController as! VoteController
        if let result = voteController.givenScore {
            let selectedIndex = tableView.indexPathForSelectedRow!.row
            model.restaurants[selectedIndex].score = (model.restaurants[selectedIndex].score + result)/2
            tableView.reloadRowsAtIndexPaths([tableView.indexPathForSelectedRow!], withRowAnimation: .Automatic)
          
         
    }
}


    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController, ontoPrimaryViewController primaryViewController: UIViewController) -> Bool {
        return true
    }
    
    
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowDetail" {
            let controller = (segue.destinationViewController as! UINavigationController).topViewController as! UITabBarController
            if let detailController = controller.viewControllers![0] as? RestaurantDetailController{
                if let voteContrl = controller.viewControllers![1] as? VoteController{
                    if let selectedRestaurantCell = sender as? RestaurantTableViewCell {
                        let indexPath = tableView.indexPathForCell(selectedRestaurantCell)!
                        let selectedRestaurant = model.restaurants[indexPath.row].restaurant
                        let selectedRestaurantScore = model.restaurants[indexPath.row].score
                        detailController.restaurant = selectedRestaurant
                        detailController.currentScore = selectedRestaurantScore
                        voteContrl.restaurant = selectedRestaurant
                }

                }

            }
        }
    }
}
