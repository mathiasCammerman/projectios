import UIKit
import MapKit
import Social

class RestaurantDetailController: UITableViewController
{
    @IBOutlet weak var restaurantName: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var openLabel: UILabel!
    @IBOutlet weak var score: ScoreControl!
   
    @IBOutlet weak var adresLabel: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var yourScore: ScoreControl!
    @IBOutlet weak var typeLabel: UILabel!
    
    var restaurant: Restaurant!
    var currentScore: Int!
    var givenScore: Int!
    var localSearchRequest:MKLocalSearchRequest!
    var localSearch:MKLocalSearch!
    var localSearchResponse:MKLocalSearchResponse!
    var error:NSError!
    var pointAnnotation:MKPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    
     override func viewDidLoad() {
        restaurantName.text = restaurant.name
        adresLabel.text = restaurant.adres
        type.text = restaurant.kitchenType
        score.rating = currentScore
        phoneNumber.text = restaurant.telephone
        
        showRestaurantOnMap(restaurant!.adres)
        showRestaurantOpen(restaurant!.closed)
    }
    
    @IBAction func save() {
        givenScore = (currentScore + yourScore.rating)/2
        score.rating = givenScore
        performSegueWithIdentifier("saved", sender: self)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.visibleViewController?.navigationItem.title = "Details"
    }
   
    func showRestaurantOnMap(adress: String){
        localSearchRequest = MKLocalSearchRequest()
        localSearchRequest.naturalLanguageQuery = adress
        localSearch = MKLocalSearch(request: localSearchRequest)
        localSearch.startWithCompletionHandler { (localSearchResponse, error) -> Void in
            
            if localSearchResponse == nil{
                let alertController = UIAlertController(title: nil, message: "Place Not Found", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
                return
            }
            
            self.pointAnnotation = MKPointAnnotation()
            self.pointAnnotation.title = adress
            self.pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude: localSearchResponse!.boundingRegion.center.longitude)
            let mapCenter = self.pointAnnotation.coordinate
            let mapCamera = MKMapCamera(lookingAtCenterCoordinate: mapCenter, fromEyeCoordinate: mapCenter, eyeAltitude: 2000)
            self.mapView.setCamera(mapCamera, animated: true)
            
            
            self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: nil)
            self.mapView.centerCoordinate = self.pointAnnotation.coordinate
            self.mapView.addAnnotation(self.pinAnnotationView.annotation!)
        }
    }
    
    func showRestaurantOpen(closed: String) {
        let formatter = NSDateFormatter()
        let currentDate = NSDate()
        formatter.locale = NSLocale.currentLocale()
        formatter.dateFormat = "EEEE"
        let convertedDate = formatter.stringFromDate(currentDate)
        
        if convertedDate == closed{
            openLabel.text = "Vandaag gesloten"
            
        }else{
            openLabel.text = "Vandaag open"
        }
        
    }
    
    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
        if !splitViewController!.collapsed {
            navigationItem.leftBarButtonItem = splitViewController!.displayModeButtonItem()
        }
    }


    @IBAction func makeReservation() {
        UIApplication.sharedApplication().openURL(NSURL(string:restaurant.website)!)
    }
    
    @IBAction func shareOnTwitter(sender: AnyObject) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter){
            let twitterSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            twitterSheet.addImage(UIImage(named: restaurant.photo)!)
            twitterSheet.setInitialText("Share on Twitter")
            self.presentViewController(twitterSheet, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Twitter account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func shareOnFacebook(sender: AnyObject) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
            let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookSheet.addImage(UIImage(named: restaurant.photo)!)
            facebookSheet.setInitialText("Share on Facebook")

            self.presentViewController(facebookSheet, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    
}