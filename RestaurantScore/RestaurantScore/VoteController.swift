import UIKit

class VoteController: UITableViewController{
    
    @IBOutlet weak var qualityScore: ScoreControl!
    @IBOutlet weak var serviceScore: ScoreControl!
    @IBOutlet weak var atmospherScore: ScoreControl!
    
    var givenScore: Int!
    var restaurant: Restaurant!

    override func viewDidLoad() {
        self.navigationItem.title = "YourTitle"
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.visibleViewController?.navigationItem.title = restaurant.name
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

    }

    
    @IBAction func save() {
        givenScore = (qualityScore.rating + serviceScore.rating + atmospherScore.rating)/3
        performSegueWithIdentifier("saved", sender: self)
    }
    
}