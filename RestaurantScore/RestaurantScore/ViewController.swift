import UIKit
import MapKit

class ViewController: UIViewController{

    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var scoreControl: ScoreControl!
    @IBOutlet weak var keukenTypeLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var adresLabel: UILabel!
    @IBOutlet weak var budgetLabel: UILabel!
    @IBOutlet weak var closedLabel: UILabel!
    @IBOutlet weak var telefoonLabel: UILabel!
    @IBOutlet weak var openLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
      var restaurant: Restaurant?
    
    
    var localSearchRequest:MKLocalSearchRequest!
    var localSearch:MKLocalSearch!
    var localSearchResponse:MKLocalSearchResponse!
    var error:NSError!
    var pointAnnotation:MKPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    
       override func viewDidLoad() {
        super.viewDidLoad()
        
       if let restaurant = restaurant {
            restaurantNameLabel.text   = restaurant.name
            scoreControl.rating = restaurant.score
            keukenTypeLabel.text = restaurant.kitchenType
            adresLabel.text = restaurant.adres
            photoImageView.image = UIImage(named:restaurant.photo)
            telefoonLabel.text = restaurant.telephone
            closedLabel.text = restaurant.closed
            budgetLabel.text = restaurant.budget
        }
        
        showRestaurantOnMap(restaurant!.adres)
        showRestaurantOpen(restaurant!.closed)

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "vote"{
       
            let voteController = segue.destinationViewController as! VoteController
            voteController.restaurant = restaurant
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showRestaurantOpen(closed: String) {
        
        let formatter = NSDateFormatter()
        let currentDate = NSDate()
        formatter.locale = NSLocale.currentLocale()
        formatter.dateFormat = "EEEE"
        let convertedDate = formatter.stringFromDate(currentDate)
        
        if convertedDate == closed{
            openLabel.text = "Vandaag gesloten"

            
            
        }else{
            openLabel.text = "Vandaag open"
        }
        
    }
    
    func showRestaurantOnMap(adress: String){
        localSearchRequest = MKLocalSearchRequest()
        localSearchRequest.naturalLanguageQuery = adress
        localSearch = MKLocalSearch(request: localSearchRequest)
        localSearch.startWithCompletionHandler { (localSearchResponse, error) -> Void in
            
            if localSearchResponse == nil{
                let alertController = UIAlertController(title: nil, message: "Place Not Found", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
                return
            }
            
            self.pointAnnotation = MKPointAnnotation()
            self.pointAnnotation.title = adress
            self.pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude: localSearchResponse!.boundingRegion.center.longitude)
            let mapCenter = self.pointAnnotation.coordinate
            let mapCamera = MKMapCamera(lookingAtCenterCoordinate: mapCenter, fromEyeCoordinate: mapCenter, eyeAltitude: 2000)
            self.mapView.setCamera(mapCamera, animated: true)

            
            self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: nil)
            self.mapView.centerCoordinate = self.pointAnnotation.coordinate
            self.mapView.addAnnotation(self.pinAnnotationView.annotation!)
        }

    }
   
}

