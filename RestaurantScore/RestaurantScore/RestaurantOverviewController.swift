import UIKit

class RestaurantOverviewController: UITableViewController {

     private var model = RestaurantScoreModel()
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("restauranttableviewcell", forIndexPath: indexPath) as! RestaurantTableViewCell
        let (restaurant, score) = model.restaurants[indexPath.row]
        cell.nameLabel.text = "\(restaurant.name)"
        cell.keukenTypeLabel.text = "\(restaurant.keukenType)"
        cell.scoreLabel.text = "\(score)"
        return cell
    }


}
