import UIKit

class RestaurantTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreControl: ScoreControl!
    @IBOutlet weak var keukenTypeLabel: UILabel!
}
