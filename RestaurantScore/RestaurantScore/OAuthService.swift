import Foundation
import OAuthSwift

class OAuthService {
    
    let properties: NSDictionary
    
    let baseUrl:String
    
    let oauthClient: OAuthSwiftClient?
    
    init() {
        //Source: splitview exercise course iOS
        let path = NSBundle.mainBundle().pathForResource("Properties", ofType: "plist")!
        properties = NSDictionary(contentsOfFile: path)!
        
        baseUrl = properties["baseUrl"] as! String
        let consumerKey = properties["consumerKey"] as! String
        let consumerSecret = properties["consumerSecret"] as! String
        let token = properties["token"] as! String
        let tokenSecret = properties["tokenSecret"] as! String
        
        oauthClient = OAuthSwiftClient(consumerKey: consumerKey, consumerSecret: consumerSecret, accessToken: token, accessTokenSecret: tokenSecret)
    }
    
    func doRequest(params: [String:String], success: (data:NSData, response: NSHTTPURLResponse) -> (), failure: (failure: NSError) -> ()) {
        oauthClient!.get("https://api.yelp.com/v2/search", parameters: params, headers: nil, success: success, failure: failure)
        
    }
    
}