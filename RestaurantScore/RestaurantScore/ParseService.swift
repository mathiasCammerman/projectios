import Foundation
import SwiftyJSON

class ParseService {
    static func parseRestaurants (data: NSData) -> [Restaurant] {
        let json = JSON(data: data)
        var restaurants: [Restaurant] = []
        
        for (_, value):(String, JSON) in json["businesses"] {
            
            let name = value["name"].string
            if name != nil  {
                restaurants.append(Restaurant(name: name!,kitchenType: "", photo: "", adres: "",about: "", telephone: "", payement: "",
                    closed: "", budget: "", website: ""))
            }
        }
        return  restaurants
    }

 
}

