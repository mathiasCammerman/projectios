My idea for this app is based on the yelp app.
The idea for my app is to show restaurants with some basic info. When you click on a restaurant 
you will see more info about the restaurant and also the location of it on a map. You can also rate
the restaurant for its quality, service and atmosphere. You are also able to share an restaurant on Facebook and Twitter.

# Things I used to make this app #
* The Swift programming language
* Basic architecture of iOS programs using common design patterns like Model-View-Controller and Delegation
* Auto Layout and Size Classes
* Storyboards, segues and common viewcontrollers like UINavigationController, UITabBarController 
* UITableView and/or UICollectionView

# Used frameworks #
* MapKit.framework
* CoreLocation.framework
* iAd.framework
* Social.framework